
#include <iostream>

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) :  x(_x),  y(_y), z(_z)
    {}
    
    void Show()
    {
        int V = x * x + y * y + z * z;

        std::cout << sqrt(V);
    }

private:
    double x;
    double y;
    double z;

};

int main()
{
    Vector hw(10,20,30);
    hw.Show();
}

